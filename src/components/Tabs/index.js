import { __ } from "@wordpress/i18n";
import classnames from "classnames";

const Tabs = ({ className, tabs, type, currentTabId, tabClick }) => {
  /**
   * Render a tabs class name
   * @param {object} tab
   * @returns buttonClasses
   */
  const renderButtonClass = (tab) => {
    const buttonClasses = classnames("ditty-tab", {
      "ditty-tab--active": tab.id === currentTabId,
    });
    return buttonClasses;
  };

  /**
   * Render a tabs content
   * @param {object} tab
   * @returns className
   */
  const renderButtonContent = (tab) => {
    return (
      <>
        {tab.icon && <span className="ditty-tab__icon">{tab.icon}</span>}
        {tab.label && <span className="ditty-tab__label">{tab.label}</span>}
      </>
    );
  };

  const classes = classnames("ditty-tabs", className, {
    "ditty-tabs--primary": type === "primary",
    "ditty-tabs--secondary": type === "secondary",
    "ditty-tabs--cloud": type === "cloud",
  });

  /**
   * Return the tabs
   */
  return (
    <div className={classes}>
      {tabs.map((tab) => {
        return (
          <button
            className={renderButtonClass(tab)}
            key={tab.id}
            onClick={() => tabClick(tab)}
          >
            {renderButtonContent(tab)}
          </button>
        );
      })}
    </div>
  );
};
export default Tabs;
